READYREFRESHTIME=1800 --Time in seconds before airframe inventories refresh.
RefreshTimer = nil
--------------------------------------
REDFORF14=SQUADRON:New("OpFor F-14", 1, "CVN-71 Tomcats")
REDFORF14.reservegroups=2
REDFORF14.maxreadygroups=1
REDFORF14.readyrefreshincrement=1
REDFORF14:AddMissionCapability({AUFTRAG.Type.INTERCEPT}, 80)
REDFORF14:AddMissionCapability({AUFTRAG.Type.CAP , AUFTRAG.Type.ESCORT}, 50)
REDFORF18=SQUADRON:New("OpFor F-18", 2, "CVN-71 Hornets")
REDFORF18.reservegroups=4
REDFORF18.maxreadygroups=2
REDFORF18.readyrefreshincrement=1
REDFORF18:AddMissionCapability({AUFTRAG.Type.INTERCEPT, AUFTRAG.Type.CAP , AUFTRAG.Type.ESCORT}, 60)
REDFORF18:AddMissionCapability({AUFTRAG.Type.SEAD}, 90)
REDFORSU33=SQUADRON:New("OpFor Su-33", 1, "Kuz Flankers")
REDFORSU33.reservegroups=5
REDFORSU33.maxreadygroups=1
REDFORSU33.readyrefreshincrement=1
REDFORSU33:AddMissionCapability({AUFTRAG.Type.INTERCEPT, AUFTRAG.Type.CAP , AUFTRAG.Type.ESCORT}, 70)

TEDDY = AIRWING:New("Teddy", "Teddy Flights")
TEDDY:SetVerbosity(4)
KUZ = AIRWING:New("AdmKuz", "Kuz Flights")
KUZ:SetVerbosity(4)

TEDDY:AddSquadron(REDFORF14)
TEDDY:AddSquadron(REDFORF18)
KUZ:AddSquadron(REDFORSU33)


local F14A2A = TEDDY:NewPayload(GROUP:FindByName("F-14 A2A"), -1, {AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT}, 100)
local F18A2A = TEDDY:NewPayload(GROUP:FindByName("F-18 A2A"), -1, {AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT}, 100)
local F18SEAD =  TEDDY:NewPayload(GROUP:FindByName("F-18 SEAD"), -1, {AUFTRAG.Type.SEAD}, 100)
local SU33A2A = KUZ:NewPayload(GROUP:FindByName("Su-33 A2A"), -1, {AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT}, 100)

TEDDY:SetMarker(false)
KUZ:SetMarker(false)
TEDDY:SetReportOn()
KUZ:SetReportOn()
TEDDY:Start()
KUZ:Start()

ZoneCAP = ZONE:New("Carrier Defense Zone") 
missionBARCAP1=AUFTRAG:NewCAP(ZoneCAP, 25000, 300)
TEDDY:AddMission(missionBARCAP1)
missionBARCAP2=AUFTRAG:NewCAP(ZoneCAP, 25000, 300)
KUZ:AddMission(missionBARCAP2)


local AgentSet=SET_GROUP:New():FilterCoalitions("red"):FilterStart()

local RedIntel=INTEL:New(AgentSet, coalition.side.red)  
RedIntel:Start() --start it

--- Function called when a NEW contact is detected.
function RedIntel:OnAfterNewContact(From, Event, To, Contact)
  local contact=Contact --Ops.Intelligence#INTEL.Contact
  ATO(contact, {TEDDY,KUZ}) --fire the function for the contact  
end --end OnAfterNew

--THIS IS A CUSTOM FUNCTION TO CREATE A MISSION FOR ALL DETECTIONS OF THE RIGHT TYPE
function ATO(contact, airwings)
  local auftrag 
  if contact.attribute == "Air_Bomber" then
      local grp= GROUP:FindByName(contact.groupname)
      local vec2 = grp:GetVec2()
      local coord = grp:GetCoordinate()
      local zoneR = ZONE_RADIUS:New("CapZone", vec2, 90000)
      auftrag=AUFTRAG:NewCAP(zoneR, 25000, 300, coord, 2700, 10)
      auftrag:SetRepeatOnFailure(5)
  
  elseif contact.attribute == "Air_Fighter" then
    local grp= GROUP:FindByName(contact.groupname)
    auftrag=AUFTRAG:NewINTERCEPT(grp)
    auftrag:SetRepeatOnFailure(5)
    auftrag:SetPriority(70, true) -- true here sets the task to cancel lower priority tasks in favor this mission
    
  elseif (contact.attribute == "Ground_SAM") or (contact.attribute == "Ground_AAA") then
    local target=GROUP:FindByName(contact.groupname)
    auftrag=AUFTRAG:NewSEAD(target, 15000)
    auftrag:SetMissionAltitude(40000)
    auftrag:SetEngageAsGroup(true)
    auftrag:SetRepeatOnFailure(25)
  end
  
  if auftrag ~= nil then
    for _, airwing in pairs(airwings) do
      airwing:AddMission(auftrag)
    end
    if RefreshTimer == nil then
      RefreshTimer = TIMER:New(refreshInventory)
      RefreshTimer:Start(READYREFRESHTIME,READYREFRESHTIME)
    end
  end
end --endfunc

function refreshInventory() 
    env.info("Refresh Called")
    if REDFORF14:CountAssetsInStock()  < REDFORF14.maxreadygroups and REDFORF14.reservegroups >= REDFORF14.readyrefreshincrement then
      TEDDY:AddAssetToSquadron(REDFORF14,REDFORF14.readyrefreshincrement)
    end
    if REDFORF18:CountAssetsInStock() < REDFORF18.maxreadygroups and REDFORF18.reservegroups >= REDFORF18.readyrefreshincrement then
      TEDDY:AddAssetToSquadron(REDFORF18,REDFORF18.readyrefreshincrement)
    end
    if REDFORSU33:CountAssetsInStock() < REDFORSU33.maxreadygroups and REDFORSU33.reservegroups >= REDFORF18.readyrefreshincrement then
      KUZ:AddAssetToSquadron(REDFORSU33,REDFORSU33.readyrefreshincrement)
    end
end


